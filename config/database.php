<?php

/**
 * Class DB
 */
class DB
{
    protected $_name;

    protected $_user;

    protected $_pass;

    protected $_host;

    protected $_port;

    public $status;

    /**
     * DB constructor.
     * @param $host
     * @param $user
     * @param $pass
     * @param $name
     * @param int $port
     */
    public function __construct($host, $user, $pass, $name, $port = 3306)
    {
        $this->_user = $user;
        $this->_pass = $pass;
        $this->_port = $port;
        $this->_name = $name;
        $this->_host = $host;
        $this->init();

    }


    /**
     * Database initialize
     */
    private function init(): void
    {
        $this->status = new mysqli(
            $this->_host,
            $this->_user,
            $this->_pass,
            $this->_name,
            $this->_port
        );

        $this->status;
    }

}


