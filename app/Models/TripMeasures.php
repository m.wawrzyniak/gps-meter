<?php

/**
 * Class TripMeasures
 */
class TripMeasures extends Model
{
    /**
     * @var string
     * @desc Table name
     */
    protected $_model = 'trip_measures';

    /**
     * @param int $tripID
     * @return string
     */
    public function getDistanceByTrip(int $tripID): string
    {
        return $this->getOne('
        SELECT MAX(distance) AS distance from ' . $this->_model . '
        WHERE trip_id = ' . $tripID . ' 
        ');

    }
}
