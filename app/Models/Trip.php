<?php


require_once 'console/ConsoleTable.php';
require_once 'app/Services/CalculateString.php';
require_once 'app/Models/TripMeasures.php';


/**
 * Class Trip
 */
class Trip extends Model
{
    /**
     * @var string
     * @desc Table name
     */
    protected $_model = 'trips';


    public function getTripsName(): void
    {
        $trips = $this->getArray(
            'SELECT id,name,measure_interval FROM ' . $this->_model
        );

        $this->initializeResults($trips);
    }

    /**
     * @param array $trips
     * @return void
     * @desc Calculate distance and maximum average speed per trip
     */
    public function initializeResults(array $trips): void
    {
        $table = new ConsoleTable();

        $table->addHeader('trip');
        $table->addHeader('distance');
        $table->addHeader('measure interval');
        $table->addHeader('avg speed');

        $tripMeasureModel = new TripMeasures();

        foreach ($trips as $key => $trip) {

            $result = [];
            $distance = [];
            $distanceString = '';
            $speed = [];

            $result[] = $this->getTripDataByName($trip['name']);

            foreach ($result[0] as $row) {
                $distance[][$key] = $row['distance'];
            }

            for ($i = 0; $i < count($distance) - 1; $i++) {
                $distanceString .= $distance[$i][$key] . '-' . $distance[$i + 1][$key] . ',';
            }

            $distanceString = substr($distanceString, 0, -1);

            $distanceArray = explode(',', $distanceString);

            $calc = new CalculateString();
            foreach ($distanceArray as $value) {
                $delta = abs($calc->calculate($value));
                $speed[] = round((3600 * $delta) / $trip['measure_interval']);

            }

            $table->addRow();

            $table->addColumn($trip['name']);
            $table->addColumn($tripMeasureModel->getDistanceByTrip($trip['id']));
            $table->addColumn($trip['measure_interval']);
            $table->addColumn(max($speed));
        }

        //Generate table
        echo "\n Listing 1.\n";
        echo "-------------------------\n";
        $table->display();


    }

    /**
     * @param string $name
     * @return array
     */
    public function getTripDataByName(string $name): array
    {
        return $this->getArray('
            SELECT * FROM trips LEFT JOIN trip_measures ON
            trips.id=trip_measures.trip_id WHERE name = "' . $name . '"
            ');
    }
}
