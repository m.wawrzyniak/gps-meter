<?php


/**
 * Class Model
 */
abstract class Model
{
    public $database;

    public function __construct()
    {
        $this->database = new DB(
            DB_HOST,
            DB_USER,
            DB_PASS,
            DB_NAME,
            DB_PORT
        );

        if ($this->database->status->connect_errno) {
            die('Database error');
        }


    }

    public function getArray(string $sql): array
    {
        $result = [];
        $query = $this->database->status->query($sql);
        while ($row = $query->fetch_assoc()) {
            $result[] = $row;
        }


        return $result;
    }

    public function getOne(string $sql): string
    {
        return $this->database->status->query($sql)->fetch_row()[0];
    }

    public function close(): void
    {
        $this->database->status->close();
    }
}
