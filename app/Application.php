<?php


require_once 'Models/Trip.php';

/**
 * Class Application
 */
final class Application
{
    //app init
    public static function start(): void
    {
        $app = new Trip();
        $app->getTripsName();
    }
}
